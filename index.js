// config esling
/* eslint-disable no-extend-native */
/* eslint-disable no-new */

// ajoute le modulo au prototype des nombres
Number.prototype.mod = function mod (n) {
  'use strict'
  return ((this % n) + n) % n
}

// parametres de jeu
const params = {
  score: 0, // le score actuel
  meilleurScore: 0, // le score actuel
  killbox: false, // les murs tuent
  vitesseJeu: 100 // vitesse du serpent
}

// GAME OVER
let gameover = false
let jeuLance = false

// taille de jeu par defaut (format 16/9)
const coords = { x: 32, y: 18 }

// preparation canvas
const canvas = document.getElementById('game')
const ctx = canvas.getContext('2d')
// const tailleCell = canvas.clientWidth / coords.x;
const tailleCell = canvas.width / coords.x

// arene virtuelle
let arene = new Array(coords.x)
  .fill(new Array(coords.y))
  .map((c) => c.fill(0))

// represente une cellule de la grille arene
class Cellule {
  // coordonees
  x = 0
  y = 0
  // element HTML
  el

  // nouvelle cellule
  constructor (x, y) {
    this.x = x
    this.y = y
  }

  // dessiner la case
  peindre () {
  }

  // indique si la case est vide/pleine
  estPlein () {
    return false
  }

  // indique si la case est mangeable
  estFruit () {
    return false
  }

  // indique si la case est un obstacle (ou la queu du serpent)
  estObstacle () {
    return false
  }
}

// represente une cellule de la queue du serpent
class Queue extends Cellule {
  // l'element suivant dans la queue
  decendant
  // l'element precedent dans la queue
  ancetre
  // anciennes coordonnees
  _x = 0
  _y = 0
  // agrandir le serpent au prochain avancement
  agrandirSuivant = false

  // dessigner la case
  peindre () {
    if (params.style === 'def') {
      let image // sprite de la case
      if (this.ancetre.x !== this.x) {
        image = document.getElementById('snek_body_x')
      } else {
        image = document.getElementById('snek_body_y')
      }
      ctx.drawImage(image, tailleCell * this.x, tailleCell * this.y, tailleCell, tailleCell)
    } else if (params.style === 'cs1') {
      ctx.fillStyle = 'green'
      ctx.fillRect(tailleCell * this.x, tailleCell * this.y, tailleCell, tailleCell)
    }
  }

  // avancer
  avancer () {
    // on passe possiblement `agrandir = true`
    // si on veut faire grandir la queue
    this._x = this.x
    this._y = this.y
    this.x = this.ancetre._x
    this.y = this.ancetre._y
    // on deplace bien le personnage dans l'arene
    arene[this.x][this.y] = this

    this.agrandir()
  }

  // agrandir la queue ou laisser une case vide
  agrandir () {
    if (this.agrandirSuivant && this.decendant === undefined) {
      // si on a pas de decendant et qu'on doir grandir, on s'en cree un
      const dec = new Queue(this._x, this._y)
      dec.ancetre = this
      // on l'ajoute bien a l'arene
      arene[this._x][this._y] = dec
      this.agrandirSuivant = false
      this.decendant = dec
    } else if (!this.agrandirSuivant && this.decendant === undefined) {
      // si on sort d'une case, la fin de la queue replace la derniere case par une cellule vide
      // note: on est forcement vide, pas possible de passer sur un fruit
      arene[this._x][this._y] = new Cellule(this._x, this._y)
    } else if (this.decendant !== undefined) {
      // on envoie la reaction en chaine si on a bien un decendant
      if (this.agrandirSuivant) this.decendant.agrandirSuivant = true
      this.decendant.avancer()
    }
    this.agrandirSuivant = false // failsafe
  }

  // indique si la case est vide/pleine
  estPlein () {
    return true
  }

  // indique si la case est un obstacle (ou la queu du serpent)
  estObstacle () {
    return true
  }
}

// represente la tete du serpent, similaire a la queue
class Tete extends Queue {
  // dessigner la case
  peindre () {
    if (params.style === 'def') {
      let image // sprite de la case
      switch (direction) {
        case 'sud':
          image = document.getElementById('snek_head_+y')
          break
        case 'est':
          image = document.getElementById('snek_head_+x')
          break
        case 'ouest':
          image = document.getElementById('snek_head_-x')
          break
        case 'nord':
        default:
          image = document.getElementById('snek_head_-y')
          break
      }
      ctx.drawImage(image, tailleCell * this.x, tailleCell * this.y, tailleCell, tailleCell)
    } else if (params.style === 'cs1') {
      ctx.fillStyle = 'orange'
      ctx.fillRect(tailleCell * this.x, tailleCell * this.y, tailleCell, tailleCell)
    }
  }

  // avancer
  avancer () {
    // remplace les anciennes coordonnees
    this._x = this.x
    this._y = this.y
    switch (direction) {
      case 'sud':
        this.y += 1
        break
      case 'est':
        this.x += 1
        break
      case 'ouest':
        this.x -= 1
        break
      case 'nord':
      default:
        this.y -= 1
        break
    }
    // logique de murs qui tuent
    if (params.killbox && (this.x > coords.x - 1 || this.y > coords.y - 1 || this.x < 0 || this.y < 0)) {
      gameOver()
    }
    // reinistialisation des coordonnees en cas de mur
    // on urilise une methode module custom rajoutee au prototype en haut du fichier
    // la raison est que Ecma ne possede pas de modulo nativement ('%' a une fonction differente)
    this.x = this.x.mod(coords.x)
    this.y = this.y.mod(coords.y)
    if (arene[this.x][this.y].estFruit()) {
      this.agrandirSuivant = true
      new Fruit()
    } else if (arene[this.x][this.y].estObstacle()) {
      gameOver()
    }

    // on deplace bien la tete dans l'arene
    arene[this.x][this.y] = this

    this.agrandir()
  }
}

// retourne des coordonnees aleatoires
function coordsAleatoires () {
  return [Math.floor(Math.random() * coords.x), Math.floor(Math.random() * coords.y)]
}

// represente un fruit pouvant etre mange par le serpent pour grandir
class Fruit extends Cellule {
  // recupere une coordonnee aleatoire pour placer le fruit et la teste
  constructor () {
    super(0, 0)

    let ok = false
    while (!ok) {
      const [x, y] = coordsAleatoires()
      if (!arene[x][y].estPlein()) {
        ok = true
        this.x = x
        this.y = y
        arene[x][y] = this
      }
    }

    this.peindre()
  }

  // dessigner la case
  peindre () {
    if (params.style === 'def') {
      ctx.drawImage(document.getElementById('fruit'), tailleCell * this.x, tailleCell * this.y, tailleCell, tailleCell)
    } else if (params.style === 'cs1') {
      ctx.fillStyle = 'red'
      ctx.fillRect(tailleCell * this.x, tailleCell * this.y, tailleCell, tailleCell)
    }
  }

  // indique si la case est vide/pleine
  estPlein () {
    return true
  }

  // indique si la case est mangeable
  estFruit () {
    return true
  }
}

// faire avancer les cellules
function avancer () {
  // fais avancer le serpent
  tete.avancer()
  // vide le canvas
  reinit()
  // peint les elements
  arene.forEach((colone) => {
    colone.forEach((el) => el.peindre())
  })
}

// direction du serpent ('nord', 'sud', 'est', 'ouest')
let direction = 'sud' // defaut vers le bas

// recuperation des entrees de touches
document.addEventListener('keydown', (e) => {
  const cle = e.key

  switch (cle) {
    case 'Z':
    case 'ArrowUp' :
      if (direction !== 'sud') { direction = 'nord' }
      break
    case 'D':
    case 'ArrowRight' :
      if (direction !== 'ouest') { direction = 'est' }
      break
    case 'S':
    case 'ArrowDown' :
      if (direction !== 'nord') { direction = 'sud' }
      break
    case 'Q':
    case 'ArrowLeft' :
      if (direction !== 'est') { direction = 'ouest' }
      break
    case ' ' :
      if (!jeuLance) lancerJeu()
      break
    default:
      break
  }
})

// puis on la remplie avec des elements Cellule
// instantiation avec les coordonnees actuelles
arene = arene
  .map((c, x) => c.map((_, y) => new Cellule(x, y)))

// on cree la tete du serpent
const tete = new Tete(16, 9)
arene[16][9] = tete

// executer une fonction `fonc()` en boucle tous les `temps` milisecondes
async function boucle (fonc, temps) {
  await setTimeout(() => {
    if (gameover) return // arret quand necessaire
    fonc()
    boucle(fonc, temps)
  }, temps)
}
// executer une fonction `fonc()` en boucle `x` fois
// avec un interval de `temps` secondes
async function boucleNum (fonc, temps, x) {
  await setTimeout(() => {
    if (gameover) return // arret quand necessaire
    fonc()
    // ferme si x == 0
    if (x > 0) boucleNum(fonc, temps, x - 1)
  }, temps)
}

// cree tous les fruits necessaires au jeu
function deploiFruits (nbr) {
  if (nbr > 0) new Fruit() // on en fait apparaitre un au debut sans delai
  if (nbr > 1) boucleNum(() => new Fruit(), 4000, nbr - 2)
}

// lancement du jeu
function lancerJeu () {
  // pour eviter de relancer en appuyant sur espace
  jeuLance = true
  gameover = false
  // on remet le titre
  document.getElementById('title').innerText = 'Colorful Snake 2'
  document.getElementById('title').classList.remove('gameover')
  // vide le canvas
  reinit()
  // lancement du serpent
  boucle(avancer, params.vitesseJeu)
  // fait apparaitre 4 fruits (au total) progressivement
  deploiFruits(3)
}

// Game Over
function gameOver () {
  jeuLance = false
  gameover = true
  document.getElementById('title').innerText = 'GAME OVER !'
  document.getElementById('title').classList.add('gameover')
}

// vide le canvas
function reinit () {
  ctx.clearRect(0, 0, canvas.width, canvas.height)
}

// Afficher un titre en gros
function afficherTitre (texte) {
  reinit()
  ctx.font = `${canvas.height * 3 / texte.length}px sans`
  ctx.fillText(texte, canvas.height * 1 / 8, canvas.height * 2 / 5)
}

// recuperation cookie
function getCookie (nomCookie) {
  const nom = nomCookie + '='
  const cookieDecode = decodeURIComponent(document.cookie)
  // pour certains caracteres comme '=S'
  const listeCookies = cookieDecode.split(';')
  // pour chaque cookie
  // on n'utilise pas forEach car on a besoin du return
  for (let i = 0; i < listeCookies.length; i++) {
    // prend le cookie actuel
    let cookie = listeCookies[i]
    // suprime les espaces inutiles
    while (cookie.charAt(0) === ' ') {
      cookie = cookie.substring(1)
    }
    // si c'est le bon cookie
    if (cookie.indexOf(nom) === 0) {
      // retourne le cookie
      return cookie.substring(nom.length, cookie.length)
    }
  }
  // sinon retourne rien
  return null
}

// definition cookie
function setCookie (nomCookie, val) {
  // s'il y a deja un cookie
  document.cookie = `${nomCookie}=${val || ''};expires=${() => {
    const d = new Date()
    d.setTime(d.getTime() + 8640000)
    return d.toUTCString()
  }};path=/`
}

// recupere les cookies necessaires
function fetchCookies () {
  // On utilise `??` pour la valeur par defaut
  params.meilleurScore = parseInt(getCookie('meilleurScore') ?? '0')
  params.killbox = getCookie('killbox') ?? params.killbox
  params.style = getCookie('style') ?? params.style
  document.getElementById('input-style').value = params.style
  params.vitesseJeu = getCookie('vitesseJeu') ?? params.vitesseJeu
  document.getElementById('input-vitesse').value = params.vitesseJeu
  document.getElementById('input-killbox').checked = params.killbox
}

// gere l'evenement `click` pour plusieurs bouttons
document.getElementById('jouer')
  .addEventListener('click', () => {
    canvas.classList.remove('cacher')
    document.getElementById('jouer').classList.add('cacher')
    document.getElementById('trailer').classList.add('cacher')
  })
document.getElementById('input-affichermenu')
  .addEventListener('click', () => {
    document.getElementsByClassName('menu-parametres')[0].classList.toggle('cacher')
  })
document.getElementById('input-killbox')
  .addEventListener('click', () => {
    params.killbox = !params.killbox
    setCookie('killbox', params.killbox)
  })
let e = document.getElementById('input-vitesse')
e.addEventListener('click', () => {
  params.vitesseJeu = e.value
  setCookie('vitesseJeu', params.vitesseJeu)
})
e = document.getElementById('input-style')
e.addEventListener('click', () => {
  params.style = e.value
  setCookie('style', params.style)
})

fetchCookies()
afficherTitre('Appuyez sur <Espace> pour commencer')
